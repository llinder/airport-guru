Ext.define('DeltaIata.model.Quiz', {
	extend : 'Ext.data.Model',

	config : {
		fields : [ {
			name : 'title',
			type : 'string'
		},{
			name : 'commonWrong',
			defaultValue: false
		},{
			name : 'index',
			defaultValue : 0
		}, {
			name : 'right',
			defaultValue : []
		}, {
			name : 'wrong',
			defaultValue : []
		}, {
			name : 'airports',
			defaultValue : []
		} ],
		
		proxy : {
			type : 'localstorage',
			id : 'quiz'
		}
	},

	getCurrent : function() {
		if (this.hasNext()) {
			return this.get('airports')[this.get('index')];
		} else {
			return null;
		}

	},

	count : function() {
		var a = this.get('airports');
		return a.length;
	},

	getScore : function() {
		return {
			right : this.get('right').length,
			wrong : this.get('wrong').length,
			total : this.get('airports').length
		}
	},

	hasNext : function() {
		var i = this.get('index');
		var a = this.get('airports');
		return i < a.length;
	},

	increment : function() {
		var i = this.get('index') + 1;
		this.set('index', i);
	},

	reset : function() {
		this.set('index', 0);
		this.set('wrong', []);
		this.set('right', []);
	}

});