Ext.define('DeltaIata.model.Airport', {
	extend : 'Ext.data.Model',
	config : {
		fields : [ {
			name : 'code'
		}, {
			name : 'name'
		}, {
			name : 'city'
		}, {
			name : 'state'
		}, {
			name : 'country'
		}, {
			name : 'latitude'
		}, {
			name : 'longitude'
		}, {
			name : 'timezone'
		}, {
			name : 'dst'
		} ]
	}
});