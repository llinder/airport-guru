Ext.define('DeltaIata.store.Quiz', {
	extend : 'Ext.data.Store',
	
	requires: ['DeltaIata.model.Quiz',
	           'Ext.data.proxy.LocalStorage'],

	config : {
		model : 'DeltaIata.model.Quiz',
		autoLoad: true,
		proxy : {
			type : 'localstorage',
			id : 'quiz'
		}
	}

});