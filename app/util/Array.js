Ext.define('DeltaIata.util.Array', {
	singleton : true,
	shuffle : function(array) {
		var counter = array.length, temp, index;

		while (counter--) {
			index = (Math.random() * (counter + 1)) | 0;

			temp = array[counter];
			array[counter] = array[index];
			array[index] = temp;
		}

		return array;
	}

})