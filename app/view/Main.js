Ext.define('DeltaIata.view.Main', {
    extend: 'Ext.navigation.View',
    xtype: 'main',
    requires: [
        'DeltaIata.view.Menu'
    ],
    config: {
        autoDestroy: false,
        itemId: 'main',

        navigationBar: {
            ui: 'dark'
        },

        items: [
            { xtype: 'menu' }
        ]
    }
});
