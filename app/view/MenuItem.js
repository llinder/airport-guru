Ext.define('DeltaIata.view.MenuItem', {
	extend : 'Ext.dataview.component.DataItem',
	xtype: 'menuitem',
	
	requires: ['Ext.Container', 'Ext.Label'],

	config : {
		title: {
			cls: 'x-menu-title',
			tpl: '<i>{title}</i> - {total} cities'
		},
		score: {
			cls: 'x-menu-score',
			tpl: 'Score: {right} correct - {wrong} incorrect'
		},
		container: {
			layout: 'vbox',
			cls: 'x-menu-item',
			items: [{
				cls: 'x-menu-buttons',
				xtype: 'container',
				layout: {type: 'hbox', pack: 'center'},
				align: 'end',
				pack: 'end'
			}]
		},
		
		launchButton: {
			text: 'Launch',
			ui: 'action',
			action: 'launch',
			margin: '10 5 10 5'
		},
		
		resetButton: {
			text: 'Reset',
			ui: 'decline',
			action: 'reset',
			margin: '10 5 10 5'
		},
		
		clearButton: {
			text: 'Clear',
			ui: 'decline',
			action: 'clear', 
			margin: '10 5 10 5',
			hidden: true
		}

	},
	
	applyContainer: function(config) {
		return Ext.factory(config, 'Ext.Container', this.getContainer());
	},
	updateContainer: function(n, o) {
		if(o) {
			this.remove(o);
		}
		if(n) {
			this.add(n);
		}
	},
	
	applyLaunchButton: function(config) {
		return Ext.factory(config, 'Ext.Button', this.getLaunchButton());
	},
	updateLaunchButton: function(n, o) {
		if(o) {
			this.getContainer().query('container')[0].remove(o);
		}
		if(n) {
			this.getContainer().query('container')[0].insert(0, n);
		}
	},
	
	applyResetButton: function(config) {
		return Ext.factory(config, 'Ext.Button', this.getResetButton());
	},
	updateResetButton: function(n, o) {
		if(o) {
			this.getContainer().query('container')[0].remove(o);
		}
		if(n) {
			this.getContainer().query('container')[0].insert(1, n);
		}
	},
	
	applyClearButton: function(config) {
		return Ext.factory(config, 'Ext.Button', this.getClearButton());
	},
	updateClearButton: function(n, o) {
		if(o) {
			this.getContainer().query('container')[0].remove(o);
		}
		if(n) {
			this.getContainer().query('container')[0].insert(2, n);
		}
	},
	
	applyTitle: function(config) {
		return Ext.factory(config, 'Ext.Label', this.getTitle());
	},
	updateTitle: function(n, o) {
		if(o) {
			this.getContainer().remove(o);
		}
		if(n) {
			this.getContainer().insert(0, n);
		}
	},
	
	applyScore: function(config) {
		return Ext.factory(config, 'Ext.Label', this.getScore());
	},
	updateScore: function(n, o) {
		if(o) {
			this.getContainer().remove(o);
		}
		if(n) {
			this.getContainer().insert(1, n);
		}
	},
	
	updateRecord: function(record) {
		if(!record) {
			return;
		}
		
		this.getTitle().setData({
			title: record.get('title'),
			total: record.get('airports').length});
		this.getScore().setData({ 
			right: record.get('right').length, 
			wrong: record.get('wrong').length
		});
		
		this.getClearButton().setHidden(!record.get('commonWrong'));
		this.getClearButton().setData(record);
		
		this.getLaunchButton().setData(record);
		this.getResetButton().setData(record);
	
		this.callParent(arguments);
	}


});