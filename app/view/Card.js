Ext.define('DeltaIata.view.Card', {
	extend : 'Ext.Container',
	xtype: 'iatacard',

	requires : [ 'Ext.picker.Picker' ],

	config : {
		layout : 'card',
		cls : 'x-card',
		choices : null,
		items : [
				{
					xtype : 'container',
					cls : 'x-card-front',
					layout : 'fit',
					items : [ {
						xtype: 'label',
						tpl: '{code}',
						centered: true,
						cls: 'x-city-code',
						paddingBottom: 300
					},{
						xtype : 'picker',
						cancelButton : false,
						modal : false,
						doneButton : 'Submit',
						height: 300,
						minHeight: 300
					} ]
				},
				{
					xtype : 'container',
					cls : 'x-card-back',
					layout : 'vbox',
					items : [
							{
								xtype : 'container',
								cls : 'x-card-back-label',
								layout : 'fit',
								flex : 1,
								tpl : [ '<h1 class="right">Correct!</h1><br/>', '<span>',
										'{code}<br/><br/>',
										'Name: {name}<br/>',
										'City: {city}<br/>',
										'Country: {country}</span>' ]
							}, {
								xtype : 'button',
								action: 'next',
								text : 'Next',
								ui : 'confirm',
								minHeight: 100,
								margin: 30
							} ]
				},
				{
					xtype : 'container',
					cls : 'x-card-back',
					layout : 'vbox',
					items : [
							{
								xtype : 'container',
								cls : 'x-card-back-label',
								layout : 'fit',
								flex : 1,
								tpl : [ '<h1 class="wrong">Wrong!</h1><br/>', '<span>',
										'{code}<br/><br/>',
										'Name: {name}<br/>',
										'City: {city}<br/>',
										'Country: {country}</span>' ]
							}, {
								xtype : 'button',
								action: 'next',
								text : 'Next',
								ui : 'decline',
								minHeight: 100,
								margin: 30
							} ]
				} ],

		listeners : [ {
			fn : 'pickerSubmitHandler',
			event : 'change',
			delegate : 'picker'
		}, {
			fn : 'nextHandler',
			event : 'tap',
			delegate : 'button[action=next]'
		} ]

	},

	updateData : function(data) {
		this.query('label[cls=x-city-code]').forEach(
				function(el, i, array) {
					el.setData(data);
				});
		this.query('container[cls=x-card-back-label]').forEach(
				function(el, i, array) {
					el.setData(data);
				});

		this.callParent(arguments);
	},

	updateChoices : function(choices) {
		this.query('picker').forEach(function(el, i, array) {
			el.setSlots({
				name : 'airport',
				data : choices
			});
			el.setValue({airport: choices[2].value});
			el.setHidden(false);
		});
	},

	pickerSubmitHandler : function(target, value, eOpts) {
		var choice = value.airport;
		this.fireEvent('answer', choice.get('code'));
	},
	
	nextHandler : function() {
		this.fireEvent('next', null);
	},

	setCorrect : function(correct) {
		var item = correct ? 1 : 2
		this.animateActiveItem(item, {
			type : 'flip'
		});
	},

	reset : function() {
		this.setActiveItem(0);
	}

});