Ext.define('DeltaIata.view.Quiz', {
	extend : 'Ext.Container',
	xtype : 'iataquiz',
	requires : [ 'DeltaIata.view.Card' ],
	
	initialize: function() {
		
		this.__scoreLabel = Ext.create('Ext.Label', {
			tpl: '{right}/{wrong}/{total}',
			cls: 'x-quiz-score',
			align: 'right'
		});
		
		this.__navBar = Ext.ComponentQuery.query('navigationview')[0].getNavigationBar();
		this.__navBar.add(this.__scoreLabel);
	
		this.callParent(arguments);
	},
	
	cleanup: function() {
		this.__navBar.remove(this.__scoreLabel);
	},

	config : {
	
		layout: 'card',
		autoDestroy: true,
		score: null,
	
		items: [{
			xtype: 'iatacard',
			itemId: 'card-1'
		},{ 
			xtype: 'iatacard',
			itemId: 'card-2'
		}]
	},
	
	updateScore: function(score) {
		this.__scoreLabel.setData(score);
	}


});