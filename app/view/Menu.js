Ext.define('DeltaIata.view.Menu', {
    extend: 'Ext.DataView',
    xtype: 'menu',

    config: {
        title: 'Delta Quiz',
        store: 'Quiz',
        defaultType: 'menuitem',
        useComponents: true
    }
});
