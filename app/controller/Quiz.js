Ext.define('DeltaIata.controller.Quiz', {
	extend : 'Ext.app.Controller',

	requires : [ 'DeltaIata.view.Card', 'DeltaIata.view.Quiz' ],
	
	views: ['Card'],

	config : {

		refs : {
			nav : '#main'
		},
		control : {
			'iatacard' : {
				answer : '__answerHandler',
				next : '__nextHandler'
			},
			'navigationview': {
				back: '__navigationBackHandler'
			}
		}

	},

	start : function(quiz) {

		var store = Ext.getStore('Quiz');
		this.__commonWrong = store.getAt(3);

		this.__initAirportDict();		
		this.__quiz = quiz;
		var quizIndex = quiz.get('index');
		// shuffle codes if starting from index 0
		if(quizIndex == 0) {
			var codes = quiz.get('airports');
			codes = DeltaIata.util.Array.shuffle(codes);
			quiz.set('airports', codes);
			quiz.save();
		}
		
		this.__cards = Ext.create('DeltaIata.view.Quiz');
		this.__cards.setScore(quiz.getScore());
		
		this.__showCard(quiz.getCurrent());

		this.getNav().push(this.__cards);
	},

	__showCard : function(code) {
		var card, choices, airport;
		airport = this.__getAirportByCode(code);
		choices = this.__getChoices(airport);
		
		this.__cardIndex = (this.__cardIndex === undefined) ? 0 : Math.abs(this.__cardIndex-1);
		card = this.__cards.getAt(this.__cardIndex);
		
		card.setChoices(choices);
		card.setData({
			code: airport.get('code'),
			name: airport.get('name'),
			city: airport.get('city'),
			country: airport.get('country')
		});
		
		this.__cards.getActiveItem().reset();
		this.__cards.animateActiveItem(this.__cardIndex, {
			type: 'slide',
			direction: 'left'
		});
	},
	
	__answerHandler: function(answer) {
		// get current city code
		var code = this.__quiz.getCurrent();
		// get airport model for code
		var airport = this.__getAirportByCode(code);
		// evaluate if the users answer was write or wrong
		var correct = (answer == airport.get('code'));
		// update quiz model
		this.__quiz.get((correct) ? 'right' : 'wrong').push(answer);
		// increment quiz index
		this.__quiz.increment();
		this.__quiz.save();
		// update view to show result
		this.__cards.getActiveItem().setCorrect(correct);
		// update score
		this.__cards.setScore(this.__quiz.getScore());
		// update commonly wrong quiz set
		if(!correct) {
			var a = this.__commonWrong.get('airports');
			var l = a.length;
			var found = false;
			for(var i=0; i<l; i++) {
				if(a[i] == code) {
					found = true;
					break;
				}
			}
			if(!found) {
				a.push(code);
				this.__commonWrong.set('airports', a);
				this.__commonWrong.save();
			}
		}
		
	},
	
	__nextHandler: function() {
		var code = this.__quiz.getCurrent();

		if(code) {
			this.__showCard(code);		
		} else {
			this.__cards.cleanup();
			this.getNav().pop();
		}
			
	},

	__getChoices : function(airport) {
		var store = Ext.getStore('Airport');
		var count = store.getCount();
		var choices = [];
		var choices_dict = {};
		choices.push({
			text : airport.get('city'),
			value : airport
		});
		choices_dict[airport.get('code')] = airport;
		while (choices.length < 5) {
			var index = Math.floor(Math.random() * count);
			var choice = store.getAt(index);
			if (choices_dict[choice.get('code')] === undefined) {
				choices.push({
					text : choice.get('city'),
					value : choice
				});
				choices_dict[choice.get('code')] = choice;
			}

		}

		return DeltaIata.util.Array.shuffle(choices);
	},
	
	__getAirportByCode: function(code) {
		return this.__dict[code];
	},
	

	__initAirportDict : function() {
		if (this.__dict === undefined) {
			var store = Ext.getStore('Airport');
			var dict = this.__dict = {};

			store.each(function(airport) {
				dict[airport.get('code')] = airport;
			});
		}

		return this.__dict;
	},
	
	__navigationBackHandler: function(navigationView, opts) {
		this.__cards.cleanup();
	}


});