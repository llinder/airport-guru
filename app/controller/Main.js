Ext.define('DeltaIata.controller.Main', {
	extend : 'Ext.app.Controller',

	config : {

		control : {
			'button[action=launch]' : {
				tap : 'launchHandler'
			},
			'button[action=reset]' : {
				tap : 'resetHandler'
			},
			'button[action=clear]' : {
				tap : 'clearHandler'
			}
		}

	},

	launchHandler : function(target) {
		var quizController = this.getApplication().getController('Quiz');
		quizController.start(target.getData());
	},

	resetHandler : function(target) {
		var quiz = target.getData();
		quiz.reset();
		quiz.save();
	},

	clearHandler : function(target) {
		var quiz = target.getData();
		quiz.reset();
		if(quiz.get('commonWrong')) {
			quiz.set('airports', []);
		}
		quiz.save();
	}

});