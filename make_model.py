#!/usr/bin/env python

import csv

def main():
	tpl = """			{{ name: "{name}", city: "{city}", country: "{country}", code: "{code}", latitude: {latitude}, longitude: {longitude}, timezone: "{timezone}", dst: "{dst}" }}""" 
	start = """
Ext.define('DeltaIata.store.Airports', {
	extend: 'Ext.data.Store',
	requires: ['DeltaIata.model.Airport'],
	config: {
		model: 'DeltaIata.model.Airport',
		storeId: 'Airports',
		data : [\n"""
	end = """]
	}

});"""

	with open('app/store/Airport.js', 'w') as o:
		with open('airports.dat', 'rb') as f:
			reader = csv.reader(f)
			b = []
			for row in reader:
				if len(row[1]) < 1 or len(row[2]) < 1 or len(row[3]) < 1 or len(row[4]) < 1:
					continue
				s = tpl.format(name=row[1],
						city=row[2],
						country=row[3],
						code=row[4],
						latitude=row[6],
						longitude=row[7],
						timezone=row[9],
						dst=row[10])
				b.append(s)
			o.write(start)
			o.write(',\n'.join(b))
			o.write(end)

if __name__ == "__main__":
    main()