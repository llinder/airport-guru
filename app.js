/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
 */

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides
//<debug>
Ext.Loader.setPath({
	'Ext' : 'touch/src'
});
Ext.Loader.setConfig({
	disableCaching : false
});
//</debug>

Ext
		.application({
			name : 'DeltaIata',

			requires : [ 'Ext.MessageBox', 'DeltaIata.util.Array' ],

			controllers : [ 'Main', 'Quiz' ],

			stores : [ 'Airport', 'Quiz' ],

			views : [ 'Main', 'MenuItem' ],

			icon : {
				'57' : 'resources/icons/Icon.png',
				'72' : 'resources/icons/Icon~ipad.png',
				'114' : 'resources/icons/Icon@2x.png',
				'144' : 'resources/icons/Icon~ipad@2x.png'
			},

			isIconPrecomposed : true,

			startupImage : {
				'320x460' : 'resources/startup/320x460.jpg',
				'640x920' : 'resources/startup/640x920.png',
				'768x1004' : 'resources/startup/768x1004.png',
				'748x1024' : 'resources/startup/748x1024.png',
				'1536x2008' : 'resources/startup/1536x2008.png',
				'1496x2048' : 'resources/startup/1496x2048.png'
			},

			launch : function() {

				// Destroy the #appLoadingIndicator element
				Ext.fly('appLoadingIndicator').destroy();
				
				// Initialize quiz data
				var quizzes = Ext.getStore('Quiz');
				quizzes.load();
				if(quizzes.getCount() == 0) {
					var all = Ext.create('DeltaIata.model.Quiz', {
						title: 'All Cities',
						airports: [ 'ALB', 'AMS', 'ANC', 'ATH', 'ATL', 'AUS', 'BOG', 'BOI', 'BOM',
						            'BOS', 'BRU', 'BUF', 'BUR', 'CLE', 'DAY', 'DEN', 'DUB', 'ELP',
						            'FAI', 'FRA', 'GUA', 'HOU', 'IND', 'IST', 'OUA', 'ACC', 'LJU',
						            'AMM', 'CUL', 'ZIH', 'KIN', 'LAS', 'LEX', 'LIM', 'LIT', 'MAD',
						            'MAN', 'MEM', 'MEX', 'MOB', 'MIA', 'MYR', 'NAS', 'OAK', 'OMA',
						            'ONT', 'PIT', 'RIC', 'ROC', 'SAN', 'SAV', 'SEA', 'SYR', 'TUL',
						            'VIE', 'CAI', 'BUD', 'BON', 'ACA', 'EDI', 'GEO', 'LAP', 'ABQ',
						            'AUA', 'BCN', 'BTR', 'BDA', 'BHM', 'BZE', 'BZN', 'CUN', 'CCS',
						            'GCM', 'GSO', 'GSP', 'GDL', 'GPT', 'GUC', 'HNL', 'HSV', 'JAN',
						            'LIR', 'CHS', 'CLT', 'MDW', 'COS', 'CMH', 'CZM', 'DFW', 'DAB',
						            'FLL', 'LGW', 'MAA', 'MHT', 'MLB', 'MKE', 'MSP', 'MBJ', 'MUC',
						            'NCE', 'NRT', 'JFK', 'LGA', 'EWR', 'ORF', 'OKC', 'PSC', 'PSP',
						            'PTY', 'CDG', 'PNS', 'PHL', 'PHX', 'PVD', 'PLS', 'RDU', 'RNO',
						            'STX', 'STT', 'SLC', 'SAT', 'SFO', 'SJC', 'SJO', 'SJU', 'SAL',
						            'SCL', 'STI', 'SNN', 'SHV', 'STR', 'TLH', 'TPA', 'BWI', 'TXL',
						            'EZE', 'ORD', 'CVG', 'CAE', 'DTW', 'RSW', 'VPS', 'BDL', 'IAH',
						            'JAX', 'MCI', 'TYS', 'LAX', 'SJD', 'SDF', 'OGG', 'MXP', 'YUL',
						            'SVO', 'BNA', 'MSY', 'PHF', 'SNA', 'MCO', 'PWM', 'PDX', 'GIG',
						            'FCO', 'SMF', 'SDQ', 'GRU', 'UVF', 'TUS', 'VCE', 'ZRH', 'ANU',
						            'FPO', 'KBP', 'DXB', 'BGI', 'BMD', 'DLA', 'YYC', 'YEG', 'YHZ',
						            'YQB', 'OTP', 'PRG', 'PUJ', 'UIO', 'DUS', 'MGA', 'TLV', 'BJX',
						            'MZT', 'LOS', 'DKR', 'JNB', 'RTB', 'TAB', 'SXM', 'SRQ', 'GEG',
						            'YVR', 'YYZ', 'EGE', 'IAD', 'DCA', 'PBI', 'NSI', 'YYG', 'YWG',
						            'YOW', 'POS', 'PVG', 'CPH', 'POP', 'GYE', 'WAW', 'SAP', 'FDF',
						            'MTY', 'ZLO', 'MID', 'PVR', 'CPT', 'ZCL', 'GGT',
						            
						            'CAK', 'AEX', 'ATW', 'AVL', 'AGS', 'BMI', 'BQK', 'BTV', 'CID',
						            'CRW', 'CHO', 'CHA', 'CSG', 'MDT', 'HTS', 'OAJ', 'EYW', 'GRK',
						            'LFT', 'LAW', 'LYH', 'MSN', 'MEI', 'MLI', 'MLU', 'EWN', 'GTR',
						            'DSM', 'DHN', 'EVV', 'XNA', 'FAY', 'FNT', 'FLO', 'FSM', 'FWA',
						            'GNV', 'GRR', 'SWF', 'PIA', 'ROA', 'SBN', 'SGF', 'TRI', 'VLD',
						            'HPN', 'ICT', 'AVP', 'ILM']});
					var nondci = Ext.create('DeltaIata.model.Quiz', {
						title: 'Non DCI Cities',
						airports: [ 'ALB', 'AMS', 'ANC', 'ATH', 'ATL', 'AUS', 'BOG', 'BOI', 'BOM',
						            'BOS', 'BRU', 'BUF', 'BUR', 'CLE', 'DAY', 'DEN', 'DUB', 'ELP',
						            'FAI', 'FRA', 'GUA', 'HOU', 'IND', 'IST', 'OUA', 'ACC', 'LJU',
						            'AMM', 'CUL', 'ZIH', 'KIN', 'LAS', 'LEX', 'LIM', 'LIT', 'MAD',
						            'MAN', 'MEM', 'MEX', 'MOB', 'MIA', 'MYR', 'NAS', 'OAK', 'OMA',
						            'ONT', 'PIT', 'RIC', 'ROC', 'SAN', 'SAV', 'SEA', 'SYR', 'TUL',
						            'VIE', 'CAI', 'BUD', 'BON', 'ACA', 'EDI', 'GEO', 'LAP', 'ABQ',
						            'AUA', 'BCN', 'BTR', 'BDA', 'BHM', 'BZE', 'BZN', 'CUN', 'CCS',
						            'GCM', 'GSO', 'GSP', 'GDL', 'GPT', 'GUC', 'HNL', 'HSV', 'JAN',
						            'LIR', 'CHS', 'CLT', 'MDW', 'COS', 'CMH', 'CZM', 'DFW', 'DAB',
						            'FLL', 'LGW', 'MAA', 'MHT', 'MLB', 'MKE', 'MSP', 'MBJ', 'MUC',
						            'NCE', 'NRT', 'JFK', 'LGA', 'EWR', 'ORF', 'OKC', 'PSC', 'PSP',
						            'PTY', 'CDG', 'PNS', 'PHL', 'PHX', 'PVD', 'PLS', 'RDU', 'RNO',
						            'STX', 'STT', 'SLC', 'SAT', 'SFO', 'SJC', 'SJO', 'SJU', 'SAL',
						            'SCL', 'STI', 'SNN', 'SHV', 'STR', 'TLH', 'TPA', 'BWI', 'TXL',
						            'EZE', 'ORD', 'CVG', 'CAE', 'DTW', 'RSW', 'VPS', 'BDL', 'IAH',
						            'JAX', 'MCI', 'TYS', 'LAX', 'SJD', 'SDF', 'OGG', 'MXP', 'YUL',
						            'SVO', 'BNA', 'MSY', 'PHF', 'SNA', 'MCO', 'PWM', 'PDX', 'GIG',
						            'FCO', 'SMF', 'SDQ', 'GRU', 'UVF', 'TUS', 'VCE', 'ZRH', 'ANU',
						            'FPO', 'KBP', 'DXB', 'BGI', 'BMD', 'DLA', 'YYC', 'YEG', 'YHZ',
						            'YQB', 'OTP', 'PRG', 'PUJ', 'UIO', 'DUS', 'MGA', 'TLV', 'BJX',
						            'MZT', 'LOS', 'DKR', 'JNB', 'RTB', 'TAB', 'SXM', 'SRQ', 'GEG',
						            'YVR', 'YYZ', 'EGE', 'IAD', 'DCA', 'PBI', 'NSI', 'YYG', 'YWG',
						            'YOW', 'POS', 'PVG', 'CPH', 'POP', 'GYE', 'WAW', 'SAP', 'FDF',
						            'MTY', 'ZLO', 'MID', 'PVR', 'CPT', 'ZCL', 'GGT' ]});
					var dci = Ext.create('DeltaIata.model.Quiz', {
						title: 'DCI Cities',
						airports: [ 'CAK', 'AEX', 'ATW', 'AVL', 'AGS', 'BMI', 'BQK', 'BTV', 'CID',
						            'CRW', 'CHO', 'CHA', 'CSG', 'MDT', 'HTS', 'OAJ', 'EYW', 'GRK',
						            'LFT', 'LAW', 'LYH', 'MSN', 'MEI', 'MLI', 'MLU', 'EWN', 'GTR',
						            'DSM', 'DHN', 'EVV', 'XNA', 'FAY', 'FNT', 'FLO', 'FSM', 'FWA',
						            'GNV', 'GRR', 'SWF', 'PIA', 'ROA', 'SBN', 'SGF', 'TRI', 'VLD',
						            'HPN', 'ICT', 'AVP', 'ILM' ]});
					var wrong = Ext.create('DeltaIata.model.Quiz', {
						title: 'Missed Cities',
						commonWrong: true});
					quizzes.add([all, nondci, dci, wrong]);
					quizzes.sync();
				}
				
				// Initialize the main view
				Ext.Viewport.add(Ext.create('DeltaIata.view.Main'));

			},

			onUpdated : function() {
				Ext.Msg
						.confirm(
								"Application Update",
								"This application has just successfully been updated to the latest version. Reload now?",
								function(buttonId) {
									if (buttonId === 'yes') {
										window.location.reload();
									}
								});
			}
		});
